extends Node

class_name ImageUtil


static func load_image(path):
	var tex_file = File.new()
	tex_file.open(path, File.READ)
	var bytes = tex_file.get_buffer(tex_file.get_len())
	var img = Image.new()
	img.load_png_from_buffer(bytes)
	return img