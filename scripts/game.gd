extends Node

# autoload singlton Game

enum CELL_METHOD { CLOSE, MARK }
var cell_method = CELL_METHOD.CLOSE

var current_image = null

var dragging = false

# inefficient
func _process(delta):
	check_win_condition()

func _input(event):
	if event is InputEventMouseButton:
		if  (event.button_index == BUTTON_LEFT and event.pressed || BUTTON_RIGHT and event.pressed):         dragging = true
		elif(event.button_index == BUTTON_LEFT and not event.pressed || BUTTON_RIGHT and not event.pressed): dragging = false
		

func check_win_condition():
	var solvable_cells = get_tree().get_nodes_in_group("solvable")

	var solved = true

	for x in solvable_cells:
		if x.state != x.TILESTATE.SOLVED:
			solved = false
	
	if solved:
		get_tree().change_scene("res://scenes/ui/gameover.tscn")
