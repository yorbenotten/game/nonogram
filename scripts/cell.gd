extends Area2D

enum TILESTATE {UNSOLVED, SOLVED, WRONG, HINTED, MARKED}
var state = TILESTATE.UNSOLVED
var grid_position = Vector2()
var solvable = false

func _ready():
	$particle.emitting = false
	$Sprite.play("unsolved")


func change_state():
	if state != TILESTATE.WRONG && state != TILESTATE.SOLVED:
		if solvable and Game.cell_method == Game.CELL_METHOD.CLOSE:
			state = TILESTATE.SOLVED
			$Sprite.play("solved")
			$particle.emitting = true
		elif Game.cell_method == Game.CELL_METHOD.MARK:
			state = TILESTATE.MARKED
			$Sprite.play("marked")
		else:
			state = TILESTATE.WRONG
			$Sprite.play("wrong")

func handle_mouse_event(mouse_input):
	if (mouse_input == 1):
		Game.cell_method = Game.CELL_METHOD.CLOSE
	elif (mouse_input == 2):
		Game.cell_method = Game.CELL_METHOD.MARK

func _on_Tile_input_event(viewport:Node, event:InputEvent, shape_idx:int):
	if (event is InputEventMouseButton && event.pressed):
		handle_mouse_event(event.button_index)
		change_state()
	if Game.dragging == true:
		change_state()
