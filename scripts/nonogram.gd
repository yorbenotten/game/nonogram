extends Node2D

onready var _cell = preload("res://scenes/cell.tscn")
onready var _tomography_label = preload("res://scenes/ui/tomography_label.tscn")

enum DIRECTION {HORIZONTAL, VERTICAL}

export(String, FILE) var nonogram_image_path

func _ready():
	load_nonogram_from_image(nonogram_image_path)

func load_nonogram_from_image(image_path):
	# load image from image file
	var image = ImageUtil.load_image(image_path)
	Game.current_image = image
	
	# get the size of the image
	var nono_size = image.get_size()

	# calculate the horizontal en vertical tomography of the nonogram
	var hor = calculate_tomography(image, nono_size)
	var ver = calculate_tomography(image, nono_size, 1)

	# calculate the size in pixels of a cell and the offset
	var nonoposition  = Math.calculate_cell_size($NonogramPanel.rect_size, nono_size)
	var cell_size = nonoposition[0].x
	var xyoffset = nonoposition[1]

	# draw everything
	draw_cells($NonogramPanel, image, _cell, cell_size, xyoffset)
	draw_tomography(_tomography_label, hor, DIRECTION.HORIZONTAL, $NonogramPanel, xyoffset, cell_size)
	draw_tomography(_tomography_label, ver, DIRECTION.VERTICAL, $NonogramPanel, xyoffset, cell_size)
	
func calculate_tomography(image, size, direction = 0, empty_color = Color.white):
	var tomography = []

	var tomo_count = 0
	var temp = []

	var width = size.y
	var height = size.x
	
	if direction == 1:
		width = size.x
		height = size.y

	image.lock()
	
	for x in range(width):
		for y in range (height):
			var pixval;
			if direction == 1:
				pixval = image.get_pixel(x,y)
			else:
				pixval = image.get_pixel(y,x)
			
			if pixval!=empty_color:
				tomo_count += 1
				continue

			if tomo_count > 0:
				temp.append(tomo_count)
				tomo_count = 0

		if tomo_count > 0:
			temp.append(tomo_count)
			tomography.append(temp)
		else:
			tomography.append(temp)
			
		tomo_count = 0
		temp = []
	image.unlock()

	return tomography
	
func draw_tomography(label, horizontal, direction, panel, xoffset, cell_size):
	for x in horizontal.size():
		for y in horizontal[x].size():
			var l = label.instance()
			var label_node = l.get_node("Label")
			label_node.text = str(horizontal[x][y])

			var offset = horizontal[x].size() - y

			if direction == DIRECTION.HORIZONTAL:
				l.rect_position = Vector2(panel.rect_position.x + -offset * cell_size, panel.rect_position.y + x * cell_size)
			if direction == DIRECTION.VERTICAL:
				l.rect_position = Vector2(panel.rect_position.x + x * cell_size, panel.rect_position.y + -offset * cell_size)

			add_child(l)

func draw_cells(panel, image, cell, cell_size, offset):
	var size = image.get_size()
	
	for x in range(size.x):
		for y in range(size.y):
			var t = cell.instance()
			t.grid_position = Vector2(x,y)
			var s = t.get_node("Sprite")
			t.scale = Vector2(cell_size/16,cell_size/16)
			t.position = Vector2(panel.rect_position.x + (cell_size * x) + offset.x, panel.rect_position.y + (cell_size * y) + offset.y)
			t.add_to_group("cell")
			image.lock()
			if image.get_pixel(x,y) != Color.white:
				t.solvable = true
				t.add_to_group("solvable")
			image.unlock()
			add_child(t)
