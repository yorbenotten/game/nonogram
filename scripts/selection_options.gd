extends Node2D

func _process(delta):
	if (Input.is_action_just_pressed("set_closed")):
		Game.cell_method = Game.CELL_METHOD.CLOSE
		var select_box = get_node("select_box")
		select_box.set_position(Vector2($closed.rect_position.x - 1, $closed.rect_position.y - 1))
	if (Input.is_action_just_pressed("set_marked")):
		Game.cell_method = Game.CELL_METHOD.MARK
		var select_box = get_node("select_box")
		select_box.rect_position = Vector2($marked.rect_position.x - 1, $marked.rect_position.y - 1)
